define(
    [
        'uiComponent',
        'Magento_Checkout/js/model/payment/renderer-list'
    ],
    function (Component,
              rendererList) {
        'use strict';
        rendererList.push(
            {
                type: 'netpay',
                component: 'Codendot_NetCommerce/js/view/payment/method-renderer/netpay-method'
            }
        );
        return Component.extend({});
    }
);