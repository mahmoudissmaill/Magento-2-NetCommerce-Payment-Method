<?php

namespace Codendot\NetCommerce\Model\Config\Source;

class PaymentMode implements \Magento\Framework\Option\ArrayInterface
{
    public function toOptionArray()
    {
        return [
            ['value' => 'test', 'label' => 'test'],
            ['value' => 'real', 'label' => 'real']
        ];
    }
}