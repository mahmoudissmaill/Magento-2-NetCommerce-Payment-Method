<?php
/**
 * NetCommerce provides online Credit Card payment processing for Visa and MasterCard cards over its secure connection channel.
 * Copyright (C) 2017  Codendot
 *
 * This file is part of Codendot/NetCommerce.
 *
 * Codendot/NetCommerce is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

namespace Codendot\NetCommerce\Model\Payment;

class NetPay extends \Magento\Payment\Model\Method\AbstractMethod
{

    protected $_code = "netpay";
    protected $_isOffline = false;

    public function isAvailable(
        \Magento\Quote\Api\Data\CartInterface $quote = null
    )
    {
        return parent::isAvailable($quote);
    }
}
