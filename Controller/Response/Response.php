<?php

namespace Codendot\NetCommerce\Controller\Response;

use Magento\Checkout\Model\Session;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;
use Magento\Sales\Model\Order;

class Response extends \Magento\Framework\App\Action\Action
{

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $_scopeConfig;

    /**
     * @var \Magento\Framework\Encryption\EncryptorInterface
     */
    protected $_encryptor;

    public function __construct(
        Context $context,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        Session $checkoutSession,
        \Magento\Framework\Locale\Resolver $store,
        \Magento\Framework\UrlInterface $urlBuilder,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Magento\Directory\Model\CountryFactory $countryFactory,
        \Magento\Framework\Encryption\EncryptorInterface $encryptor
    )
    {
        parent::__construct($context);
        $this->_scopeConfig = $scopeConfig; //Used for getting data from System/Admin config
        $this->checkoutSession = $checkoutSession; //Used for getting the order: $order = $this->checkoutSession->getLastRealOrder(); And other order data like ID & amount
        $this->store = $store; //Used for getting store locale if needed $language_code = $this->store->getLocale();
        $this->urlBuilder = $urlBuilder; //Used for creating URLs to other custom controllers, for example $success_url = $this->urlBuilder->getUrl('frontname/path/action');
        $this->resultJsonFactory = $resultJsonFactory; //Used for returning JSON data to the afterPlaceOrder function ($result = $this->resultJsonFactory->create(); return $result->setData($post_data);)
        $this->_encryptor = $encryptor;
    }

    public function execute()
    {
        $post = $this->getRequest()->getPostValue();

        $txtIndex = $post["txtIndex"];
        $txtAmount = $post["txtAmount"];
        $txtCurrency = $post["txtCurrency"];
        $txtMerchNum = $post["txtMerchNum"];
        $txtNumAut = $post["txtNumAut"];
        $RespVal = $post["RespVal"];
        $RespMsg = $post["RespMsg"];

        $arr_querystring = array(
            'RespVal' => $post["RespVal"],
            'txtIndex' => $post["txtIndex"],
            'txtAmount' => $post["txtAmount"],
            'txtCurrency' => $post["txtCurrency"],
            'RespMsg' => $post["RespMsg"],
            'txtNumAut' => $post["txtNumAut"],
            'violation' => ''
        );

        $sha_key = $this->_getSHAKey();
        $signature = hash('SHA256', $txtMerchNum . $txtIndex . $txtAmount . $txtCurrency . $txtNumAut . $RespVal . $RespMsg . $sha_key);
        $nc_signature = $this->getRequest()->get("signature");

        $params = array('_secure' => true, '_query' => $arr_querystring);

        if ($signature == $nc_signature) {
            // no security violation detected!
            if ($this->getRequest()->get("RespVal") == '1') {
                return $this->_redirection('success', $params);

            } else {
                return $this->_redirection('failure', $params);
            }
        } else {
            //security violation was detected!
            $arr_querystring = array(
                'RespVal' => '',
                'txtIndex' => '',
                'txtAmount' => '',
                'txtCurrency' => '',
                'RespMsg' => '',
                'txtNumAut' => '',
                'violation' => "security violation was detected!"
            );
            $params = array('_secure' => true, '_query' => $arr_querystring);
            return $this->_redirection('failure', $params);
        }
    }

    /**
     * [_redirection redirect to specify page]
     * @param  [string] $type  
     * @param  [array] $params 
     * @return [string]        
     */
    protected function _redirection($type, $params)
    {
        switch ($type) {
            case 'success':
                $route = 'netcommerce/success/success';
                break;
            case 'failure':
                $route = 'netcommerce/failure/failure';
                break;
        }
        $url = $this->urlBuilder->getUrl($route, $params);
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        $resultRedirect->setPath($url);
        return $resultRedirect;
    }

    /**
     * [_getSHAKey Get SHA Key]
     * @return [string] [scret key between NetCommerce and the merchant]
     */
    protected function _getSHAKey()
    {
        $configPath = 'payment/netpay/sha_key';
        $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORES;
        $encryptSHAKey = $this->_scopeConfig->getValue($configPath, $storeScope);
        $decryptSHAKey = $this->_encryptor->decrypt($encryptSHAKey);
        return $decryptSHAKey;
    }
}