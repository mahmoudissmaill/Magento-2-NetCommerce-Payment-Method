<?php

namespace Codendot\NetCommerce\Controller\Success;

use Magento\Checkout\Model\Session;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;

class Success extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;

    /**
     * @var \Magento\Sales\Model\Service\InvoiceService
     */
    protected $_invoiceService;

    /**
     * @var \Magento\Framework\DB\Transaction
     */
    protected $_transaction;

    /**
     * @var \Magento\Sales\Model\OrderFactory
     */
    protected $_orderFactory;

    /**
     * @var \Magento\Framework\Message\ManagerInterface
     */
    protected $_invoiceSender;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $_scopeConfig;

    /**
     * @var \Magento\Framework\Message\ManagerInterface
     */
    protected $_messageManager;

    /**
     * @var \Magento\Sales\Model\Order\Email\Sender\OrderSender
     */
    protected $orderSender;

    public function __construct(
        Context $context,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\UrlInterface $urlBuilder,
        \Magento\Sales\Model\OrderFactory $orderFactory,
        \Magento\Sales\Model\Service\InvoiceService $invoiceService,
        \Magento\Framework\DB\Transaction $transaction,
        \Magento\Sales\Model\Order\Email\Sender\InvoiceSender $invoiceSender,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Magento\Sales\Model\Order\Email\Sender\OrderSender $orderSender
    )
    {
        parent::__construct($context);
        $this->logger = $logger;
        $this->_scopeConfig = $scopeConfig; //Used for getting data from System/Admin config
        $this->urlBuilder = $urlBuilder;
        $this->_orderFactory = $orderFactory;
        $this->_invoiceService = $invoiceService;
        $this->_transaction = $transaction;
        $this->_invoiceSender = $invoiceSender;
        $this->_messageManager = $messageManager;
        $this->orderSender = $orderSender;
    }

    public function execute()
    {
        $post_data = $this->getRequest()->getParams();

        $orderId = $post_data['txtIndex'];//order id for which want to create invoice
        $order = $this->_orderFactory->create()->loadByIncrementId($orderId);
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        $params = array('_secure' => true, '_query' => $post_data);
        if ($order->canInvoice()) {
            try {
                $invoice = $this->_invoiceService->prepareInvoice($order);
                $invoice->setRequestedCaptureCase(\Magento\Sales\Model\Order\Invoice::CAPTURE_ONLINE);
                $invoice->register();
                $invoice->save();
                $transactionSave = $this->_transaction->addObject(
                    $invoice
                )->addObject(
                    $invoice->getOrder()
                );
                $transactionSave->save();
                $this->_invoiceSender->send($invoice);
                //send notification code
                $orderStateNew = \Magento\Sales\Model\Order::STATE_NEW;

                $configPath = 'payment/netpay/order_status';
                $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORES;
                $status = $this->_scopeConfig->getValue($configPath, $storeScope);
                $orderStatePayAccepted = $status;

                $order->setState($orderStateNew);
                $order->setStatus($orderStatePayAccepted);
                $order->addStatusHistoryComment(
                    __('Notified customer about invoice #%1.', $invoice->getId())
                );
                $order->setIsCustomerNotified(true);
                $order->save();

                /**
                if ($order->getCanSendNewEmailFlag()) {
                    try {
                        $this->orderSender->send($order);
                    } catch (\Exception $e) {
                        $this->logger->critical($e);
                    }
                }
                **/

                $url = $this->urlBuilder->getUrl('checkout/onepage/success', $params);
                $resultRedirect->setPath($url);
                return $resultRedirect;
            } catch (\Exception $e) {
                $order->addStatusHistoryComment('Exception message: ' . $e->getMessage(), false);
                $order->save();
                return null;
            }
        } else {
            $this->_messageManager->addErrorMessage(__('Cannot create invoice for your order due an error occurred'));
            $resultRedirect->setPath('checkout/cart');
            return $resultRedirect;
        }
    }
}