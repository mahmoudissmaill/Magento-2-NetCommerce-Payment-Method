<?php

namespace Codendot\NetCommerce\Controller\Failure;

use Magento\Checkout\Model\Session;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;

class Failure extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;

    /**
     * @var \Magento\Sales\Model\OrderFactory
     */
    protected $_orderFactory;

    /**
     * @var \Magento\Framework\Message\ManagerInterface
     */
    protected $_messageManager;

    /**
     * @var \Magento\Sales\Model\Order\Email\Sender\OrderSender
     */
    protected $orderSender;

    public function __construct(
        Context $context,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Framework\UrlInterface $urlBuilder,
        \Magento\Sales\Model\OrderFactory $orderFactory,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Magento\Sales\Model\Order\Email\Sender\OrderSender $orderSender
    )
    {
        parent::__construct($context);
        $this->logger = $logger;
        $this->urlBuilder = $urlBuilder;
        $this->_orderFactory = $orderFactory;
        $this->_messageManager = $messageManager;
        $this->orderSender = $orderSender;
    }

    public function execute()
    {
        $post_data = $this->getRequest()->getParams();

        $orderId = $post_data['txtIndex'];//order id for which want to create invoice
        $order = $this->_orderFactory->create()->loadByIncrementId($orderId);
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        $params = array('_secure' => true, '_query' => $post_data);
        try {

            $orderStateCanceled = \Magento\Sales\Model\Order::STATE_CANCELED;
            $orderStateCanceledPayment = \Magento\Sales\Model\Order::STATE_CANCELED;

            $order->setState($orderStateCanceled);
            $order->setStatus($orderStateCanceledPayment);
            $order->addStatusHistoryComment(
                __('Gateway has declined the payment.')
            );
            $order->setIsCustomerNotified(true);
            $order->save();

            /**
            if ($order->getCanSendNewEmailFlag()) {
                try {
                    $this->orderSender->send($order);
                } catch (\Exception $e) {
                    $this->logger->critical($e);
                }
            }
            **/

            $this->_messageManager->addErrorMessage(__('Sorry, your order is canceled due gateway has declined payment, either of your card information (billing address or card validation digits) don\'t match. Please try again'));
            $url = $this->urlBuilder->getUrl('checkout/onepage/failure', $params);
            $resultRedirect->setPath($url);
            return $resultRedirect;
        } catch (\Exception $e) {
            $order->addStatusHistoryComment('Exception message: ' . $e->getMessage(), false);
            $order->save();
            $this->_messageManager->addErrorMessage(__($e->getMessage()));
            $resultRedirect->setPath('checkout/cart');
            return $resultRedirect;
        }
    }
}