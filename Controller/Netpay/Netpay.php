<?php

namespace Codendot\NetCommerce\Controller\Netpay;

use Magento\Checkout\Model\Session;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;
use Magento\Sales\Model\Order;

class Netpay extends \Magento\Framework\App\Action\Action
{

    /**
     * @var \Magento\Sales\Model\OrderFactory
     */
    protected $_orderFactory;

    /**
     * @var \Magento\Framework\Pricing\PriceCurrencyInterface
     */
    protected $_priceCurrency;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $_scopeConfig;

    /**
     * @var \Magento\Framework\Encryption\EncryptorInterface
     */
    protected $_encryptor;

    public function __construct(
        Context $context,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        Session $checkoutSession,
        \Magento\Sales\Model\OrderFactory $orderFactory,
        \Magento\Framework\Locale\Resolver $store,
        \Magento\Framework\UrlInterface $urlBuilder,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Magento\Directory\Model\CountryFactory $countryFactory,
        \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency,
        \Magento\Framework\Encryption\EncryptorInterface $encryptor
    )
    {
        parent::__construct($context);
        $this->_scopeConfig = $scopeConfig; //Used for getting data from System/Admin config
        $this->checkoutSession = $checkoutSession; //Used for getting the order: $order = $this->checkoutSession->getLastRealOrder(); And other order data like ID & amount
        $this->_orderFactory = $orderFactory;
        $this->store = $store; //Used for getting store locale if needed $language_code = $this->store->getLocale();
        $this->urlBuilder = $urlBuilder; //Used for creating URLs to other custom controllers, for example $success_url = $this->urlBuilder->getUrl('frontname/path/action');
        $this->resultJsonFactory = $resultJsonFactory; //Used for returning JSON data to the afterPlaceOrder function ($result = $this->resultJsonFactory->create(); return $result->setData($post_data);)
        $this->_countryFactory = $countryFactory;
        $this->_priceCurrency = $priceCurrency;
        $this->_encryptor = $encryptor;
    }

    public function execute()
    {
        //Your custom code for getting the data the payment provider needs
        //Structure your return data so the netpay-form-builder.js can build your form correctly
        $post_data = $this->_getOrderData();
        $result = $this->resultJsonFactory->create();

        return $result->setData($post_data); //return data in JSON format
    }


    /**
     * Get order object
     *
     * @return \Magento\Sales\Model\Order
     */
    protected function getOrder()
    {
        return $this->_orderFactory->create()->loadByIncrementId(
            $this->checkoutSession->getLastRealOrderId()
        );
    }

    /**
     * [getOrderId get Order Id]
     * @return [integer] [Order Id]
     */
    protected function getOrderId()
    {
        return $this->checkoutSession->getLastRealOrderId();
    }

    /**
     * [getIncrementId Get Increment Id for order]
     * @return [integer] [id of the order]
     */
    protected function getIncrementId()
    {
        return $this->getOrder()->getIncrementId();
    }

    /**
     * [getOrderInformation Order Information]
     * @param  [object] $_order [Order Object]
     * @return [array]         [Order Data]
     */
    protected function getOrderInformation($_order)
    {
        $data = array();
        $data['first_name'] = $_order->getBillingAddress()->getFirstname();
        $data['last_name'] = $_order->getBillingAddress()->getLastname();
        $data['email'] = $_order->getBillingAddress()->getEmail();
        $data['customer_id'] = $_order->getCustomerId();
        $data['mobile'] = $_order->getBillingAddress()->getTelephone();
        $data['address_line1'] = $_order->getBillingAddress()->getStreet(1);
        $data['address_line2'] = $_order->getBillingAddress()->getStreet(2);
        $data['city'] = $_order->getBillingAddress()->getCity();
        $data['country_id'] = $_order->getBillingAddress()->getCountryId();
        $data['postal_code'] = $_order->getBillingAddress()->getPostcode();
        $data['country'] = $this->_getCountryname($_order->getBillingAddress()->getCountryId());
        return $data;
    }

    /**
     * [_getOrderData Get Order Data like address,total,currency...]
     * @return [array] [array of data]
     */
    protected function _getOrderData()
    {
        $order = $this->getOrder();
        $orderInfo = $this->getOrderInformation($order);
        $txtAmount = number_format($order->getBaseGrandTotal(), 2, '.', '');
        $floatet_txtAmount = number_format((float)$txtAmount, 2, '.', '');  // Outputs like -> 105.00
        $txtCurrency = $this->_getCurrentCurrencyCode() == 'USD' ? '840' : '422'; // USD: 840, LBP: 422
        $txtMerchNum = $this->_getMerchantNumber();
        $txtIndex = $this->getOrderId();
        $txthttp = $this->urlBuilder->getUrl('netcommerce/response/response');
        $sha_key = $this->_getSHAKey();
        $payment_mode = $this->_getPaymentMode();
        $signature = hash('SHA256', $txtAmount . $txtCurrency . $txtIndex . $txtMerchNum . $txthttp . $sha_key);
        $data = array(
            'action' => $this->_getGatewayURL(),
            'fields' => array(
                'payment_mode' => $payment_mode,
                'txtAmount' => $txtAmount,
                'txtCurrency' => $txtCurrency,
                'txtIndex' => $txtIndex,
                'txtMerchNum' => $txtMerchNum,
                'txthttp' => $txthttp,
                'signature' => $signature,
                'first_name' => $orderInfo['first_name'],
                'last_name' => $orderInfo['last_name'],
                'email' => $orderInfo['email'],
                'customer_id' => $orderInfo['customer_id'],
                'mobile' => $orderInfo['mobile'],
                'address_line1' => $orderInfo['address_line1'],
                'address_line2' => $orderInfo['address_line2'],
                'city' => $orderInfo['city'],
                'country' => $orderInfo['country'],
                'postal_code' => $orderInfo['postal_code'],
                'country_id' => $orderInfo['country_id'],
                'Lng' => strtoupper(strstr($this->store->getLocale(), '_', true))
            )
        );
        return $data;
    }

    /**
     * [_getCountryname Get Country Name]
     * @param  [string] $countryCode [Country Code]
     * @return [string]              [Country Name]
     */
    protected function _getCountryname($countryCode)
    {
        $country = $this->_countryFactory->create()->loadByCode($countryCode);
        return $country->getName();
    }

    /**
     * Get current currency code
     *
     * @return string
     */
    protected function _getCurrentCurrencyCode()
    {
        return $this->_priceCurrency->getCurrency()->getCurrencyCode();
    }

    /**
     * [_getGatewayURL Get Gateway URl]
     * @return [string] [Gateway URL]
     */
    protected function _getGatewayURL()
    {
        $configPath = 'payment/netpay/merchant_gateway_url';
        $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORES;
        return $this->_scopeConfig->getValue($configPath, $storeScope);
    }

    /**
     * [_getMerchantNumber Get Merchant Number]
     * @return [string] [Gateway Mercahnt Number]
     */
    protected function _getMerchantNumber()
    {
        $configPath = 'payment/netpay/merchant_nb';
        $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORES;
        return $this->_scopeConfig->getValue($configPath, $storeScope);
    }

    /**
     * [_getPaymentMode Get Payment Mode]
     * @return [string] [test for testing, real for real account]
     */
    protected function _getPaymentMode()
    {
        $configPath = 'payment/netpay/payment_mode';
        $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORES;
        return $this->_scopeConfig->getValue($configPath, $storeScope);
    }

    /**
     * [_getSHAKey Get SHA Key]
     * @return [string] [scret key between NetCommerce and the merchant]
     */
    protected function _getSHAKey()
    {
        $configPath = 'payment/netpay/sha_key';
        $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORES;
        $encryptSHAKey = $this->_scopeConfig->getValue($configPath, $storeScope);
        $decryptSHAKey = $this->_encryptor->decrypt($encryptSHAKey);
        return $decryptSHAKey;
    }
}