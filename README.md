# Magento2-NetCommerce-Payment-Method


**Magento 2 Create Payment Method** proves that store admin has rights to generate as many payment methods as they need when your store is based on Magento 2 platform, an great era of ecommerce architecture. Depending on the customer's requirement, you probably plug it in your list of the existing payment method. The additional payment methods surely bring the diversity of customer choice when they proceed to checkout on your site. On the other's hands, multiple payment method is the great strategy to reach out the global marketplace.

In the tutorial, you will learn how to create own Payment Gateway integration in Magento 2 stores. After launching the new payment methods, you will find and configure it according the path `Admin panel > Stores > Settings > Configuration > Sales > Payment Methods`. There, admin possibly assigns a payment method to specific shipping method, this means they will work in pairs when enabling.

This Module is integrated with NetCommerce that provides online Credit Card payment processing for Visa and MasterCard cards over its secure connection channel.


## Step 1: Create payment method module

1. Create file registration.php

``` php
<?php
\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Codendot_NetCommerce',
    __DIR__
);
```

2. Declare `module.xml` file

``` xml
<?xml version="1.0" ?>
<config xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
        xsi:noNamespaceSchemaLocation="urn:magento:framework:Module/etc/module.xsd">
    <module name="Codendot_NetCommerce" setup_version="1.0.0"/>
</config>
```

## Step 2: Declare payment method module

1. Now we create file `payment.xml` file in `etc` folder etc/payment.xml

``` xml
<?xml version="1.0" ?>
<payment xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:noNamespaceSchemaLocation="urn:magento:module:Magento_Payment:etc/payment.xsd">
    <groups>
        <group id="offline">
            <label>Offline Payment Methods</label>
        </group>
    </groups>
    <methods>
        <method name="netpay">
            <allow_multiple_address>1</allow_multiple_address>
        </method>
    </methods>
</payment>
```

2. Create `config.xml` file in `etc` folder.

``` xml
<?xml version="1.0" ?>
<config xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
        xsi:noNamespaceSchemaLocation="urn:magento:module:Magento_Store:etc/config.xsd">
    <default>
        <payment>
            <netpay>
                <active>1</active>
                <model>Codendot\NetCommerce\Model\Payment\NetPay</model>
                <order_status>pay_netcommerce</order_status>
                <title>NetCommerce</title>
                <merchant_gateway_url>https://www.netcommercepay.com/iPAY/default.asp</merchant_gateway_url>
                <sha_key>TEST</sha_key>
                <merchant_nb>01999131</merchant_nb>
                <payment_mode>test</payment_mode>
                <allowspecific>0</allowspecific>
                <can_use_checkout>1</can_use_checkout>
            </netpay>
        </payment>
    </default>
</config>
```

In this file, we declare Model of this payment method, we calle `NetPay` model.

3. Create NetPay model file

Create file Model/Payment/NetPay.php

``` php
<?php
namespace Codendot\NetCommerce\Model\Payment;

class NetPay extends \Magento\Payment\Model\Method\AbstractMethod
{

    protected $_code = "netpay";
    protected $_isOffline = false;

    public function isAvailable(
        \Magento\Quote\Api\Data\CartInterface $quote = null
    )
    {
        return parent::isAvailable($quote);
    }
}
```

## Step 3: Display payment method in checkout page

In previous steps, we talked about declare module, config and model files. Now we need to display this Simple payment method in checkout page.

1. Create layout file: view/frontend/layout/checkout_index_index.xml

``` xml
<?xml version="1.0" ?>
<page layout="1column" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xsi:noNamespaceSchemaLocation="urn:magento:framework:View/Layout/etc/page_configuration.xsd">
    <body>
        <referenceBlock name="checkout.root">
            <arguments>
                <argument name="jsLayout" xsi:type="array">
                    <item name="components" xsi:type="array">
                        <item name="checkout" xsi:type="array">
                            <item name="children" xsi:type="array">
                                <item name="steps" xsi:type="array">
                                    <item name="children" xsi:type="array">
                                        <item name="billing-step" xsi:type="array">
                                            <item name="children" xsi:type="array">
                                                <item name="payment" xsi:type="array">
                                                    <item name="children" xsi:type="array">
                                                        <item name="renders" xsi:type="array">
                                                            <item name="children" xsi:type="array">
                                                                <item name="netpay" xsi:type="array">
                                                                    <item name="component" xsi:type="string">
                                                                        Codendot_NetCommerce/js/view/payment/netpay
                                                                    </item>
                                                                    <item name="methods" xsi:type="array">
                                                                        <item name="netpay" xsi:type="array">
                                                                            <item name="isBillingAddressRequired"
                                                                                  xsi:type="boolean">true
                                                                            </item>
                                                                        </item>
                                                                    </item>
                                                                </item>
                                                            </item>
                                                        </item>
                                                    </item>
                                                </item>
                                            </item>
                                        </item>
                                    </item>
                                </item>
                            </item>
                        </item>
                    </item>
                </argument>
            </arguments>
        </referenceBlock>
    </body>
</page>
```


2. Create js files to load KO template in checkout page

- File: /view/frontend/web/js/view/payment/netpay.js

``` js
define(
    [
        'uiComponent',
        'Magento_Checkout/js/model/payment/renderer-list'
    ],
    function (Component,
              rendererList) {
        'use strict';
        rendererList.push(
            {
                type: 'netpay',
                component: 'Codendot_NetCommerce/js/view/payment/method-renderer/netpay-method'
            }
        );
        return Component.extend({});
    }
);
```

In this file, it call another component: `js/view/payment/method-renderer/netpay-method`


Now we need to create `/view/frontend/web/js/view/payment/method-renderer/netpay-method.js`

What this does is to call your custom controller after the customer clicked the Place Order button.

In the custom controller, you get all the form data that you need to post.

``` js
define(
    [
        'jquery',
        'Magento_Checkout/js/view/payment/default',
        'mage/url',
        'Magento_Customer/js/customer-data',
        'Magento_Checkout/js/model/error-processor',
        'Magento_Checkout/js/model/full-screen-loader',
        'Codendot_NetCommerce/js/netCommerce/netpay-form-builder'
    ],
    function ($, Component, url, customerData, errorProcessor, fullScreenLoader, formBuilder) {
        'use strict';
        return Component.extend({
            redirectAfterPlaceOrder: false, //This is important, so the customer isn't redirected to success.phtml by default
            defaults: {
                template: 'Codendot_NetCommerce/payment/netpay'
            },
            getMailingAddress: function () {
                return window.checkoutConfig.payment.checkmo.mailingAddress;
            },
            afterPlaceOrder: function () {
                var custom_controller_url = url.build('netcommerce/netpay/netpay'); //your custom controller url
                $.post(custom_controller_url, 'json')
                    .done(function (response) {
                        customerData.invalidate(['cart']);
                        formBuilder(response).submit(); //this function builds and submits the form
                    })
                    .fail(function (response) {
                        errorProcessor.process(response, this.messageContainer);
                    })
                    .always(function () {
                        fullScreenLoader.stopLoader();
                    });
            }

        });
    }
);
```

3. After that you call the `netpay-form-builder` function and submit/redirect the customer to the payment provider's URL.

Now we need to create `/view/frontend/web/js/view/netCommerce/netpay-form-builder.js`

```
define(
    [
        'jquery',
        'underscore',
        'mage/template'
    ],
    function ($, _, mageTemplate) {
        'use strict';

        /* This is the form template used to generate the form, from your Controller JSON data */
        var form_template =
            '<form action="<%= data.action %>" method="POST" hidden enctype="application/x-www-form-urlencoded">' +
            '<% _.each(data.fields, function(val, key){ %>' +
            '<input value="<%= val %>" name="<%= key %>" type="hidden">' +
            '<% }); %>' +
            '</form>';
        return function (response) {
            var form = mageTemplate(form_template);
            var final_form = form({
                data: {
                    action: response.action, //notice the "action" key is the form action you return from controller
                    fields: response.fields //fields are inputs of the form returned from controller
                }
            });
            return $(final_form).appendTo($('[data-container="body"]')); //Finally, append the form to the <body> element (or more accurately to the [data-container="body"] element)
        };
    }
);
```

4. Finally, create KO template file /view/frontend/web/template/payment/netpay.html

``` php
<div class="payment-method" data-bind="css: {'_active': (getCode() == isChecked())}">
    <div class="payment-method-title field choice">
        <input type="radio"
               name="payment[method]"
               class="radio"
               data-bind="attr: {'id': getCode()}, value: getCode(), checked: isChecked, click: selectPaymentMethod, visible: isRadioButtonVisible()"/>
        <label data-bind="attr: {'for': getCode()}" class="label"><span data-bind="text: getTitle()"></span></label>
    </div>
    <img src="https://www.netcommercepay.com/logo/NCseal_S.gif" border="0" alt="NetCommerce Security Seal">
    <div style="margin-bottom: 10px;color:red;"
         data-bind="i18n: 'You will be redirected directly to NetCommerce Secure Payment Page after placing your order where he keys-in his credit card information and submits the order for processing.'"></div>
    <div class="payment-method-content">
        <!-- ko foreach: getRegion('messages') -->
        <!-- ko template: getTemplate() --><!-- /ko -->
        <!--/ko-->
        <div class="payment-method-billing-address">
            <!-- ko foreach: $parent.getRegion(getBillingAddressFormName()) -->
            <!-- ko template: getTemplate() --><!-- /ko -->
            <!--/ko-->
        </div>
        <div class="checkout-agreements-block">
            <!-- ko foreach: $parent.getRegion('before-place-order') -->
            <!-- ko template: getTemplate() --><!-- /ko -->
            <!--/ko-->
        </div>
        <div class="actions-toolbar">
            <div class="primary">
                <button class="action primary checkout"
                        type="submit"
                        data-bind="
                        click: placeOrder,
                        attr: {title: $t('Place Order')},
                        css: {disabled: !isPlaceOrderActionAllowed()},
                        enable: (getCode() == isChecked())
                        "
                        disabled>
                    <span data-bind="i18n: 'Place Order'"></span>
                </button>
            </div>
        </div>
    </div>
</div>
```

## Preview
![Screenshot](screenshot/NetCommerce-Preview.jpg)